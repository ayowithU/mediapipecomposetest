package com.example.mediapipecompose

import android.Manifest
import android.os.Bundle
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.mediapipecompose.mediapipe.MediaPipePoseDetect
import com.example.mediapipecompose.ui.theme.MediaPipeComposeTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionRequired
import com.google.accompanist.permissions.rememberPermissionState
import com.google.mediapipe.components.PermissionHelper

class MainActivity : ComponentActivity() {
    private val mediaPipePostDetect = MediaPipePoseDetect(this)

    @ExperimentalPermissionsApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MediaPipeComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    val preview = remember {
                        LinearLayout(this, null)
                    }

                    mediaPipePostDetect.setupPreviewDisplayView(preview)
                    mediaPipePostDetect.initializeNativeAssetManager()
                    mediaPipePostDetect.initProcessor()

                    AndroidView(
                        modifier = Modifier.fillMaxSize(),
                        factory = {
                            preview
                        }
                    ) {
                        it.clipToOutline = true
                    }
                }
            }
        }
        PermissionHelper.checkAndRequestCameraPermissions(this)
    }

    override fun onResume() {
        super.onResume()
        mediaPipePostDetect.onResume()
        mediaPipePostDetect.startCamera(this)
        if (PermissionHelper.cameraPermissionsGranted(this)) {
            mediaPipePostDetect.startCamera(this)
        }
    }

    override fun onPause() {
        super.onPause()
        mediaPipePostDetect.onPause()
    }
}
