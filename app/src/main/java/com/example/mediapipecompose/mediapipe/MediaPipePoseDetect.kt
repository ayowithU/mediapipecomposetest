package com.example.mediapipecompose.mediapipe

import android.app.Activity
import android.content.Context
import android.graphics.SurfaceTexture
import android.util.Log
import android.util.Size
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import com.google.mediapipe.components.CameraHelper
import com.google.mediapipe.components.CameraHelper.CameraFacing
import com.google.mediapipe.components.ExternalTextureConverter
import com.google.mediapipe.components.FrameProcessor
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList
import com.google.mediapipe.framework.AndroidAssetUtil
import com.google.mediapipe.framework.Packet
import com.google.mediapipe.framework.PacketGetter
import com.google.mediapipe.glutil.EglManager
import com.google.protobuf.InvalidProtocolBufferException

class MediaPipePoseDetect(
    private val context: Context,
    private val binaryGraph: String = "pose_tracking_gpu.binarypb",
    private val inputVideoStream: String = "input_video",
    private val outputVideoStream: String = "output_video",
    private val outputLandmarksStream: String = "pose_landmarks",
    private val cameraFacing: CameraFacing = CameraFacing.BACK,
    // Flips the camera-preview frames vertically before sending them into FrameProcessor to be
    // processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
    // This is needed because OpenGL represents images assuming the image origin is at the bottom-left
    // corner, whereas MediaPipe in general assumes the image origin is at top-left.
    private val shouldFlipFramesVertically: Boolean = true,
    private val mediaPipeJni: String = "mediapipe_jni",
    // Creates and manages an {@link EGLContext}.
    private val eglManager: EglManager = EglManager(null) // todo not sure what this does exactly
) {

    // {@link SurfaceView} that displays the camera-preview frames processed by a MediaPipe graph.
    private val previewDisplayView: SurfaceView by lazy { SurfaceView(context) } // todo does this belong here?

    // Sends camera-preview frames into a MediaPipe graph for processing, and displays the processed
    // frames onto a {@link Surface}.
    private val processor: FrameProcessor by lazy {
        FrameProcessor(
            context, eglManager.nativeContext, binaryGraph, inputVideoStream, outputVideoStream
        )
    }

    // Handles camera access
    private val cameraHelper: Camera2Helper by lazy {
        Camera2Helper(
            context,
            CustomSurfaceTexture(65)
        )
    }

    // Converts the GL_TEXTURE_EXTERNAL_OES texture from Android camera into a regular texture to be
    // consumed by {@link FrameProcessor} and the underlying MediaPipe graph.
    private lateinit var converter: ExternalTextureConverter

    // {@link SurfaceTexture} where the camera-preview frames can be accessed.
    private lateinit var previewFrameTexture: SurfaceTexture // todo does this belong here?

    companion object {
        private const val TAG = "MediaPipePoseDetect"
    }

    init {
        // Load all native libraries needed by the app.
        System.loadLibrary(mediaPipeJni)
    }

    private fun getLandmarksDebugString(landmarks: NormalizedLandmarkList): String {
        var landmarksString = ""
        for ((landmarkIndex, landmark) in landmarks.landmarkList.withIndex()) {
            landmarksString += "Landmark[$landmarkIndex]: (${landmark.x}, ${landmark.y}, ${landmark.z})"
        }
        return landmarksString
    }

    /**
     * METHODS CALLED IN ON CREATE
     */
    fun setupPreviewDisplayView(viewGroup: ViewGroup) {
        previewDisplayView.visibility = View.GONE
        viewGroup.addView(previewDisplayView)
        previewDisplayView.holder.addCallback(
            object : SurfaceHolder.Callback {
                override fun surfaceCreated(holder: SurfaceHolder) {
                    processor.videoSurfaceOutput.setSurface(holder.surface)
                }

                override fun surfaceChanged(
                    holder: SurfaceHolder,
                    format: Int,
                    width: Int,
                    height: Int
                ) {
                    onPreviewDisplaySurfaceChanged(holder, format, width, height)
                }

                override fun surfaceDestroyed(holder: SurfaceHolder) {
                    processor.videoSurfaceOutput.setSurface(null)
                }
            })
    }

    fun initializeNativeAssetManager() {
        AndroidAssetUtil.initializeNativeAssetManager(context)
    }

    fun initProcessor() {
        processor.videoSurfaceOutput.setFlipY(shouldFlipFramesVertically)
        processor.addPacketCallback(outputLandmarksStream) { packet: Packet ->
            Log.v(TAG, "Received multi-hand landmarks packet.")
            Log.v(TAG, packet.toString())
            val landmarksRaw = PacketGetter.getProtoBytes(packet)
            try {
                val landmarks = NormalizedLandmarkList.parseFrom(landmarksRaw)
                if (landmarks == null) {
                    Log.v(TAG, "[TS:" + packet.timestamp + "] No iris landmarks.")
                    return@addPacketCallback
                }
                // Note: If eye_presence is false, these landmarks are useless.
                Log.v(
                    TAG,
                    "[TS:" +
                        packet.timestamp +
                        "] #Landmarks for iris: " +
                        landmarks.landmarkCount
                )
                Log.v(TAG, getLandmarksDebugString(landmarks))
            } catch (e: InvalidProtocolBufferException) {
                Log.e(TAG, "Couldn't Exception received - $e")
                return@addPacketCallback
            }
        }
    }

    /**
     * -----------------------------
     */

    /**
     * LIFECYCLE
     */

    fun onResume() {
        converter = ExternalTextureConverter(
            eglManager.context, 2
        )
        converter.setFlipY(shouldFlipFramesVertically)
        converter.setConsumer(processor)
    }

    fun onPause() {
        converter.close()
        // Hide preview display until we re-open the camera again.
        previewDisplayView.visibility = View.GONE
        cameraHelper.cameraDevice?.close()
    }

    /**
     * -------------
     */

    fun startCamera(context: Context) {
        cameraHelper.setOnCameraStartedListener(
            CameraHelper.OnCameraStartedListener { surfaceTexture: SurfaceTexture? ->
                previewFrameTexture =
                    surfaceTexture ?: throw IllegalStateException("surfaceTexture is null")
                // Make the display view visible to start showing the preview. This triggers the
                // SurfaceHolder.Callback added to (the holder of) previewDisplayView.
                previewDisplayView.visibility = View.VISIBLE
                Log.d(TAG, "Camera Started")
            }
        )
        cameraHelper.startCamera(context as Activity, cameraFacing, /*surfaceTexture=*/null)
    }

    /**
     *TODO these are view methods, do they belong here?
     */

    private fun onPreviewDisplaySurfaceChanged(
        holder: SurfaceHolder?,
        format: Int,
        width: Int,
        height: Int
    ) {
        // (Re-)Compute the ideal size of the camera-preview display (the area that the
        // camera-preview frames get rendered onto, potentially with scaling and rotation)
        // based on the size of the SurfaceView that contains the display.
        val viewSize = computeViewSize(width, height)
        val displaySize = cameraHelper.computeDisplaySizeFromViewSize(viewSize)
        val isCameraRotated = cameraHelper.isCameraRotated

        // Connect the converter to the camera-preview frames as its input (via
        // previewFrameTexture), and configure the output width and height as the computed
        // display size.
        converter.setSurfaceTextureAndAttachToGLContext(
            previewFrameTexture,
            if (isCameraRotated) displaySize.height else displaySize.width,
            if (isCameraRotated) displaySize.width else displaySize.height
        )
    }

    private fun computeViewSize(width: Int, height: Int): Size {
        return Size(width, height)
    }
}
